﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeoozeDemo
{
    public class EmailHelper
    {
        public void SendEmail()
        {
            Connect();
            //code to send email
            Console.WriteLine("Email Sent");
            Disconnect();
        }

        private void Connect()
        {
            Console.WriteLine("Connected");
        }

        private void Disconnect()
        {
            Console.WriteLine("Disconnected");
        }
    }
}
