﻿using System;

namespace CodeoozeDemo
{
    class Program
    {
        static void Main(string[] args)
        {

            //Product product = new Product();

            //product.SetPrice(10);
            //var price = product.GetPrice();
            //Console.WriteLine(price);

            //EmailHelper emailHelper = new EmailHelper();
            //emailHelper.SendEmail();

            //Student student = new Student("Paul", 30, "Tokyo", "Tokyo School");
            //student.PrintStudentDetails();


            //Numbers numbers = new Numbers();
            //numbers.Add(10, 50);
            //numbers.Add(10, 50, 40);

            Shape[] shapes = new Shape[2];
            shapes[0] = new Rectangle();
            shapes[1] = new Circle();

            for(int i=0; i < shapes.Length; i++)
            {
                shapes[i].Draw();
            }

            Console.ReadLine();
        }
    }
}
