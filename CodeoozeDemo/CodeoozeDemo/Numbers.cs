﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeoozeDemo
{
    public class Numbers
    {
        public void Add(int a, int b, int c)
        {
            Console.WriteLine($"Sum is: {a + b + c}");
        }

        public void Add(int a, int b)
        {
            Console.WriteLine($"Sum is: {a + b}");
        }
    }
}
