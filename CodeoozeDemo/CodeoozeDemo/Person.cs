﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeoozeDemo
{
    public class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string City { get; set; }

        public Person(string name, int age, string city)
        {
            this.Name = name;
            this.Age = age;
            this.City = city;
        }

        public void PrintDetails()
        {
            Console.WriteLine($"Name: {Name}");
            Console.WriteLine($"Age: {Age}");
            Console.WriteLine($"City: {City}");

        }
    }

    public class Student : Person
    {
        public string School { get; set; }

        public Student(string name, int age, string city, string school) :base(name, age, city)
        {
            this.School = school;
        }

        public void PrintStudentDetails()
        {
            base.PrintDetails();
            Console.WriteLine($"School: {School}");
        }
    }

}
