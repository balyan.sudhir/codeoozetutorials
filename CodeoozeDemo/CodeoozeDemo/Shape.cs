﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeoozeDemo
{
    public class Shape
    {
        public virtual void Draw()
        {

        }
    }

    public class Rectangle : Shape
    {
        public override void Draw()
        {
            base.Draw();
            Console.WriteLine("Rectangle");
        }
    }

    public class Circle : Shape {
        public override void Draw()
        {
            base.Draw();
            Console.WriteLine("Circle");
        }
    }
}
