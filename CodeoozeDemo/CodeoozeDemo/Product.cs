﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeoozeDemo
{
    public class Product
    {
        private float Price { get; set; }

        public void SetPrice(float price)
        {
            if (price > 0)
                this.Price = price;
            //Throw exception
        }

        public float GetPrice()
        {
            return Price;
        }
    }
}
